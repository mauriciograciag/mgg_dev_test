﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Accounts")]
    public class AccountsController : Controller
    {
        // GET: api/Accounts
        [HttpGet]
        public JsonResult Get()
        {
            List<Account> lstAccounts;

            lstAccounts = AccountsFacade.GetAll();

            return Json(lstAccounts);
        }

        /*
        // GET: api/Accounts/5
        [HttpGet("{acctName}", Name = "Get")]
        public JsonResult Get(String acctName)
        {
            Account resp ;

            resp = AccountsFacade.FindByName(acctName);

            return Json(resp);
        }

        // POST: api/Accounts
        [HttpPost]
        public void Post([FromBody]string acctName)
        {
            Account acc;
            bool success;

            acc = new Account()
            {
                Id = Guid.NewGuid(),
                Name = acctName,
                CurrentBalance = 0,
                CreatedDate = DateTime.Now,
                //AccountType = TODO enumeration
            };

            success = AccountsFacade.Register(acc);
        }

        // PUT: api/Accounts/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        */
    }
}
