USE [TransFraud]
GO

/*
Account

1	C1231006815
1	C1666544295
1	C1305486145
1	C553264065
2	M1979787155
2	M2044282225
*/
INSERT INTO [dbo].[Account]
           ([Id]
           ,[AccountTypeId]
           ,[Name]
           ,[CurrentBalance]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,(SELECT ID FROM [dbo].[AccountType] WHERE [description] = 'CHECKING')
           ,'M1979787155'
           ,0
           ,GETDATE()
           ,NULL)
GO

INSERT INTO [dbo].[Account]
           ([Id]
           ,[AccountTypeId]
           ,[Name]
           ,[CurrentBalance]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,(SELECT ID FROM [dbo].[AccountType] WHERE [description] = 'CHECKING')
           ,'M2044282225'
           ,0
           ,GETDATE()
           ,NULL)
GO



/*****************/

INSERT INTO [dbo].[Account]
           ([Id]
           ,[AccountTypeId]
           ,[Name]
           ,[CurrentBalance]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,(SELECT ID FROM [dbo].[AccountType] WHERE [description] = 'SAVING')
           ,'C1231006815'
           ,0
           ,GETDATE()
           ,NULL)
GO

INSERT INTO [dbo].[Account]
           ([Id]
           ,[AccountTypeId]
           ,[Name]
           ,[CurrentBalance]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,(SELECT ID FROM [dbo].[AccountType] WHERE [description] = 'SAVING')
           ,'C1666544295'
           ,0
           ,GETDATE()
           ,NULL)
GO

INSERT INTO [dbo].[Account]
           ([Id]
           ,[AccountTypeId]
           ,[Name]
           ,[CurrentBalance]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,(SELECT ID FROM [dbo].[AccountType] WHERE [description] = 'SAVING')
           ,'C1305486145'
           ,0
           ,GETDATE()
           ,NULL)
GO

INSERT INTO [dbo].[Account]
           ([Id]
           ,[AccountTypeId]
           ,[Name]
           ,[CurrentBalance]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,(SELECT ID FROM [dbo].[AccountType] WHERE [description] = 'SAVING')
           ,'C553264065'
           ,0
           ,GETDATE()
           ,NULL)
GO

