USE [TransFraud]
GO

/****** Object:  Table [dbo].[Transaction]    Script Date: 07/06/2018 07:14:49 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Transaction](
	[Id] [uniqueidentifier] NOT NULL,
	[Step] [int] NOT NULL,
	
	[TransactionTypeId] [uniqueidentifier] NOT NULL,

	[OrigAccountId] [uniqueidentifier] NOT NULL,
	[OrigPrevBalance] [decimal](12, 3) NOT NULL,
	[OrigNewBalance] [decimal](12, 3) NOT NULL,
	
	[Amount] [decimal](12, 3) NOT NULL,
	
	[DestAccountId] [uniqueidentifier] NOT NULL,
	[DestPrevBalance] [decimal](12, 3) NOT NULL,
	[DestNewBalance] [decimal](12, 3) NOT NULL,
	
	[IsFraud] [bit] NOT NULL,
	[IsFlaggedFraud] [bit] NOT NULL,
	
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Account_Dest] FOREIGN KEY([DestAccountId])
REFERENCES [dbo].[Account] ([Id])
GO

ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Account_Dest]
GO

ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Account_Orig] FOREIGN KEY([OrigAccountId])
REFERENCES [dbo].[Account] ([Id])
GO

ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Account_Orig]
GO

ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_TransactionType] FOREIGN KEY([TransactionTypeId])
REFERENCES [dbo].[TransactionType] ([Id])
GO

ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_TransactionType]
GO


