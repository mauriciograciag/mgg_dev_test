USE [TransFraud]
GO

/*

TransactionType

1	PAYMENT
2	DEBIT
3	TRANSFER
4	CASH_OUT

*/
INSERT INTO [dbo].[TransactionType]
           ([Id]
           ,[Description]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,'PAYMENT'
           ,GETDATE(),
		   NULL)
GO

INSERT INTO [dbo].[TransactionType]
           ([Id]
           ,[Description]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,'DEBIT'
           ,GETDATE(),
		   NULL)
GO

INSERT INTO [dbo].[TransactionType]
           ([Id]
           ,[Description]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,'TRANSFER'
           ,GETDATE(),
		   NULL)
GO

INSERT INTO [dbo].[TransactionType]
           ([Id]
           ,[Description]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,'CASH_OUT'
           ,GETDATE(),
		   NULL)
GO


