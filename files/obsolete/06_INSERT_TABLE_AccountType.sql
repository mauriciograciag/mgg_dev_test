USE [TransFraud]
GO

/*
1	CHECKING
2	SAVING
*/
INSERT INTO [dbo].[AccountType]
           ([Id]
           ,[Description]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,'CHECKING'
           ,GETDATE()
           ,NULL)
GO

INSERT INTO [dbo].[AccountType]
           ([Id]
           ,[Description]
           ,[CreatedDate]
           ,[UpdatedDate])
     VALUES
           (NEWID()
           ,'SAVING'
           ,GETDATE()
           ,NULL)
GO


