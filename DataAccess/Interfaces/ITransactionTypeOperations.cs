﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Defines the main operations for the Transaction types
    /// </summary>
    public interface ITransactionTypeOperations
    {
        /// <summary>
        /// Get all the transaction types
        /// </summary>
        /// <returns></returns>
        IEnumerable<TransactionType> GetAll();
    }
}
