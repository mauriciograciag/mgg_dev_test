﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Defines the 3 main operations for the Transactions
    /// </summary>
    public interface ITransOperations
    {
        /// <summary>
        /// Registers a transaction and returns true when successfull
        /// </summary>
        /// <param name="t">The transaction to register</param>
        /// <returns>true if operation was successful</returns>
        bool Register(Transaction t);

        /// <summary>
        /// Searches for a transaction by a given filter
        /// </summary>
        /// <param name="isFraudFilter">optional parameter for the isFraud column</param>
        /// <param name="NameDest">Name of the destination account, it allows substring search</param>
        /// <param name="dateStart">The start of the Date range</param>
        /// <param name="dateStart">The end of the Date range</param>
        /// <returns>The list of the results that might be a empty IEnumerable</returns>
        IEnumerable<Transaction> Search(bool ? isFraudFilter, string nameDest, DateTime dateStart, DateTime dateEnd);
 
        /// <summary>
        /// Updates only the fraude information of a given transaction
        /// </summary>
        /// <param name="isFraud"></param>
        /// <param name="isFlagedFraud"></param>
        /// <returns></returns>
        bool UpdateFraudInfo(bool isFraud, bool isFlagedFraud, Guid transactionId);

        
    }
}
