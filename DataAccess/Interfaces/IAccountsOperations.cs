﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Defines the main operations for the Accounts
    /// </summary>
    public interface IAccountsOperations
    {
        /// <summary>
        /// Registers an account
        /// </summary>
        /// <param name="acc"></param>
        /// <returns></returns>
        bool Register(Account acc);

        /// <summary>
        /// Get all the accounts
        /// </summary>
        /// <returns></returns>
        IEnumerable<Account> GetAll();

        /// <summary>
        /// Finds and account by exact name
        /// </summary>
        /// <param name="accountName"></param>
        /// <returns></returns>
        Account GetByName (string accountName);

        /// <summary>
        /// Removes the given account 
        /// </summary>
        /// <param name="acc"></param>
        /// <returns>true when succesful</returns>
        bool Remove(Account acc);
        
    }
}
