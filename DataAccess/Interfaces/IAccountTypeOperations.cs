﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Defines the main operations for the Accounts
    /// </summary>
    public interface IAccountTypeOperations
    {
        /// <summary>
        /// Get all the accounts
        /// </summary>
        /// <returns></returns>
        IEnumerable<AccountType> GetAll();
        bool Register(AccountType accType);
    }
}
