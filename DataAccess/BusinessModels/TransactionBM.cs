﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class TransactionBM : Transaction 
    {
        public TransactionBM(Transaction t)
        {
            decimal aux;

            this.Id = Guid.NewGuid();
            this.Amount = t.Amount;
            this.CreatedDate = DateTime.Now;

            //Substract the amount from the origin
            this.OrigAccountId = t.OrigAccountId;
            aux = this.OrigNewBalance;
            this.OrigNewBalance = t.OrigPrevBalance - t.Amount;
            this.OrigPrevBalance = aux;

            //Add the amount from the destination
            this.DestAccountId = t.DestAccountId;
            aux = this.DestNewBalance;
            this.DestNewBalance = this.DestPrevBalance + t.Amount;
            this.DestPrevBalance = aux;



            this.IsFlaggedFraud = false;
            this.IsFraud = false;

        }
    }
}
