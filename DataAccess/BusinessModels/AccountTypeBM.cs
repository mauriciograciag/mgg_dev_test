﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class AccountTypeBM : AccountType
    {
        public static AccountType CreateAccountType(string accTypeName)
        {
            AccountType accType;

            accType = new AccountType()
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                Description = accTypeName,
            };

            return accType;
        }
    }
}
