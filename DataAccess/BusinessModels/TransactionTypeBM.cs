﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class TransactionTypeBM : TransactionType
    {
        public static TransactionType CreateTransactionType(string v)
        {
            TransactionType transType;

            transType = new TransactionType()
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
            };

            return transType;
        }
    }
}
