﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class AccountBM : Account
    {
        /// <summary>
        /// This is a helper method to create accounts on the fly
        /// </summary>
        /// <param name="acctName"></param>
        /// <returns></returns>
        public static Account CreateAccount(String acctName)
        {
            Account acc;
            AccountType accType;

            //Por defecto todas las cuentas son de ahorro
            accType = AccountTypesFacade.GetAll().FirstOrDefault(x => x.Description.Equals("SAVINGS")) ;

            if(accType == null)
            {
                accType = new AccountType()
                {
                    Id = Guid.NewGuid(),
                    CreatedDate = DateTime.Now,
                    Description = "SAVINGS",
                };

                AccountTypesFacade.Register(accType);

            }

            acc = new Account()
            {
                Id = Guid.NewGuid(),
                Name = acctName,
                CurrentBalance = 0,
                CreatedDate = DateTime.Now,
                AccountTypeId = accType.Id,
            };

            return acc;
        }
    }
}
