﻿using BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    internal class AccountOperationsMock : IAccountsOperations
    {
        private static IList<Account> lstAccounts = new List<Account>();

        
        public IEnumerable<Account> GetAll()
        {
            if ((lstAccounts != null) && (lstAccounts.Count == 0))
            {
                lstAccounts.Add(AccountBM.CreateAccount("Diana"));
                lstAccounts.Add(AccountBM.CreateAccount("Mauricio"));
                lstAccounts.Add(AccountBM.CreateAccount("Pedro"));

            }
            return lstAccounts;
        }

        public Account GetByName(string accountName)
        {
            return lstAccounts.FirstOrDefault(x => x.Name.Equals(accountName));
        }

        public bool Register(Account acc)
        {
            bool success;

            success = false;

            try
            {
                lstAccounts.Add(acc);
                success = true;
            }
            catch
            {

            }

            return success;
        }

        public bool Remove(Account acc)
        {
            return lstAccounts.Remove(acc);
        }


    }
}
