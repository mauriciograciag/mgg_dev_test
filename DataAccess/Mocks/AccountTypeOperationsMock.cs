﻿using BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    internal class AccountTypeOperationsMock : IAccountTypeOperations
    {
        private static IList<AccountType> lstAccTypes = new List<AccountType>();

        public IEnumerable<AccountType> GetAll()
        {
            if ((lstAccTypes != null) && (lstAccTypes.Count == 0))
            {
                lstAccTypes.Add(AccountTypeBM.CreateAccountType("SAVINGS"));
                lstAccTypes.Add(AccountTypeBM.CreateAccountType("CHECKING"));

            }
            return lstAccTypes;
        }

        public AccountType GetByName(string accountName)
        {
            return lstAccTypes.Where(x => x.Description.Equals(accountName)).FirstOrDefault();
        }

        public bool Register(AccountType acc)
        {
            bool success;

            success = false;

            try
            {
                lstAccTypes.Add(acc);
                success = true;
            }
            catch
            {

            }

            return success;
        }

        public bool Remove(AccountType acc)
        {
            return lstAccTypes.Remove(acc);
        }


    }
}
