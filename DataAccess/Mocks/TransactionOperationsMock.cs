﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    internal class TransactionOperationsMock : ITransOperations
    {
        private static IList<Transaction> lstTransactions = new List<Transaction>();

        /// <summary>
        /// Registers a transaction and returns true when successfull
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool Register(Transaction t)
        {
            bool success;

            success = false;

            if((t != null) && (t.Id == null))
            {
                t.Id = Guid.NewGuid();
            }

            try
            {
                lstTransactions.Add(t);
                success = true;
            }
            catch
            {
                //sucess stays false 
            }

            return success ;
        }

        /// <summary>
        /// Searches for a transaction by a given filter
        /// </summary>
        /// <param name="isFraudFilter">optional parameter for the isFraud column</param>
        /// <param name="NameDest">Name of the destination account, it allows substring search</param>
        /// <param name="dateStart">The start of the Date range</param>
        /// <param name="dateStart">The end of the Date range</param>
        ///
        /// <returns></returns>
        public IEnumerable<Transaction> Search(bool? isFraudFilter, string nameDest, DateTime dateStart, DateTime dateEnd)
        {
            IEnumerable<Transaction> searchResults;

            searchResults = lstTransactions;

            //If the isFraudFilter was specified
            if (isFraudFilter.HasValue)
            {
                searchResults = searchResults.Where(t => t.IsFraud == isFraudFilter.Value);
            }

            //TODO

            if( ! string.IsNullOrEmpty(nameDest))
            {
                //make the join to Account using DestAccountId to check if th string provided as filter is contained in the account name
                //searchResults = searchResults.Where(t => t.DestAccountId
            }

            //TODO 
            //Check that the ttransaction is between the given dates 

            return searchResults;
        }

        /// <summary>
        /// Updates only the fraud information of a given transaction
        /// </summary>
        /// <param name="isFraud">is fraud</param>
        /// <param name="isFlagedFraud">is the transaction flagged as fraud</param>
        /// <param name="transactionId">Guid of the transaction to be updated</param>
        /// <returns></returns>
        public bool UpdateFraudInfo(bool isFraud, bool isFlagedFraud, Guid transactionId)
        {
            Transaction trans;
            bool updated;

            updated = true;

            try
            {
                trans = lstTransactions.Where(t => t.Id.Equals(transactionId)).FirstOrDefault();


                if (trans != null)
                {
                    trans.IsFraud = isFraud;
                    trans.IsFlaggedFraud = isFlagedFraud;
                    updated = true;
                }
                //Todo update the TRANS in the repository/db
            }
            catch
            {

            }

            return updated;
        }
    }
}
