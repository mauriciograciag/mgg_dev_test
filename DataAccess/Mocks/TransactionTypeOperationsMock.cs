﻿using BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    internal class TransactionTypeOperationsMock : ITransactionTypeOperations
    {
        private static IList<TransactionType> lstTransTypes = new List<TransactionType>();

        public IEnumerable<TransactionType> GetAll()
        {
            if ((lstTransTypes != null) && (lstTransTypes.Count == 0))
            {
                lstTransTypes.Add(TransactionTypeBM.CreateTransactionType("SAVINGS"));
                lstTransTypes.Add(TransactionTypeBM.CreateTransactionType("CHECKING"));

            }

            return lstTransTypes;
        }

        
    }
}
