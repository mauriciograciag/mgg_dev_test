﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    /// <summary>
    /// This is a Facade static class to encapsulte the actual implementation of the storage mechanism
    /// </summary>

    public static class TransactionsFacade 
    {
        private static ITransOperations transImpl = new TransactionOperationsMock();

        /// <summary>
        /// Registers a transaction and returns true when successfull
        /// </summary>
        /// <param name="t">The transaction to register</param>
        /// <returns>true if operation was successful</returns>
        public static bool Register(Transaction t)
        {
            return transImpl.Register(t);
        }

        /// <summary>
        /// Searches for a transaction by a given filter
        /// </summary>
        /// <param name="isFraudFilter">optional parameter for the isFraud column</param>
        /// <param name="NameDest">Name of the destination account, it allows substring search</param>
        /// <param name="dateStart">The start of the Date range</param>
        /// <param name="dateStart">The end of the Date range</param>
        /// <returns>The list of the results that might be a empty IEnumerable</returns>
        public static IEnumerable<Transaction> Search(bool? isFraudFilter, string nameDest, DateTime dateStart, DateTime dateEnd)
        {
            return transImpl.Search(isFraudFilter, nameDest, dateStart, dateEnd);
        }

        /// <summary>
        /// Updates only the fraude information of a given transaction
        /// </summary>
        /// <param name="isFraud"></param>
        /// <param name="isFlagedFraud"></param>
        /// <returns></returns>
        public static bool UpdateFraudInfo(bool isFraud, bool isFlagedFraud, Guid transactionId)
        {
            return transImpl.UpdateFraudInfo(isFraud, isFlagedFraud, transactionId);
        }
    }
}
