﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    /// <summary>
    /// This is a Facade static class to encapsulte the actual implementation of the storage mechanism
    /// </summary>
    public static class AccountTypesFacade 
    {
        /// <summary>
        /// Dependency Injection with on the fly injection
        /// </summary>
        private static IAccountTypeOperations accImpl = (DataAccessSettings.IsMocking) ? (IAccountTypeOperations)new AccountTypeOperationsMock() : new AccountTypeOperationsImpl();

        public static IEnumerable<AccountType> GetAll()
        {
            return accImpl.GetAll();
        }

        public static bool Register(AccountType accType)
        {
            return accImpl.Register(accType);
        }
    }
}
