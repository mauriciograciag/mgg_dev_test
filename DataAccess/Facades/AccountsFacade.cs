﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    /// <summary>
    /// This is a Facade static class to encapsulte the actual implementation of the storage mechanism
    /// </summary>
    public static class AccountsFacade 
    {
        /// <summary>
        /// Dependency Injection with on the fly injection
        /// </summary>
        private static IAccountsOperations accImpl = (DataAccessSettings.IsMocking) ? (IAccountsOperations)new AccountOperationsMock() : new AccountOperationsImpl();

        public static bool Register(Account acc)
        {
            return accImpl.Register(acc);
        }

        public static List<Account> GetAll()
        {
            return accImpl.GetAll().ToList();
        }

        public static Account FindByName(string accountName)
        {
            return accImpl.GetByName(accountName);
        }
        public static bool Remove(Account acc)
        {
            return accImpl.Remove(acc);
        }

    }
}
