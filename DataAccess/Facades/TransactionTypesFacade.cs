﻿using System;
using System.Collections.Generic;

namespace DataAccess
{
    /// <summary>
    /// This is a Facade static class to encapsulte the actual implementation of the storage mechanism
    /// </summary>
    public static class TransactionTypesFacade
    {
        /// <summary>
        /// Dependency Injection with on the fly injection
        /// </summary>
        private static ITransactionTypeOperations ttImpl = (DataAccessSettings.IsMocking) ? (ITransactionTypeOperations)new TransactionTypeOperationsMock() : new TransactionTypeOperationsImpl();

        public static IEnumerable<TransactionType> GetAll()
        {
            return ttImpl.GetAll();
        }

    }
}
