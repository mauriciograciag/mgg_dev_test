﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Actual implementation for the AccounType data access
    /// </summary>
    internal class AccountTypeOperationsImpl : IAccountTypeOperations
    {
        public IEnumerable<AccountType> GetAll()
        {
            IList<AccountType> lstAccountTypes;

            lstAccountTypes = new List<AccountType>();

            try
            {
                using (var db = new TransEntities())
                {
                    lstAccountTypes = db.AccountType.ToList();
                }
            }
            catch (Exception)
            {

            }


            return lstAccountTypes;
        }

        public bool Register(AccountType accType)
        {
            bool success;

            success = false;


            try
            {
                using (var db = new TransEntities())
                {
                    db.AccountType.Add(accType);
                    db.SaveChanges();
                    success = true;
                }
            }
            catch (Exception)
            {

            }
            

            return success;
        }
    }
}
