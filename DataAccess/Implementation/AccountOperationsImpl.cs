﻿using BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    internal class AccountOperationsImpl : IAccountsOperations
    {
        private static IList<Account> lstAccounts = new List<Account>();

        public bool Register(Account acc)
        {
            bool success;

            success = false;

            try
            {
                lstAccounts.Add(acc);
                success = true;
            }
            catch
            {

            }

            return success;
        }

        public IEnumerable<Account> GetAll()
        {


            return lstAccounts;
        }

        public Account GetByName(string accountName)
        {
            return lstAccounts.FirstOrDefault(x => x.Name.Equals(accountName));
        }

        public bool Remove(Account acc)
        {
            return lstAccounts.Remove(acc);
        }


    }
}
