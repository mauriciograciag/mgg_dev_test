﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Actual implementation for the TransactionTypesImpl data access
    /// </summary>
    internal class TransactionTypeOperationsImpl : ITransactionTypeOperations
    {
        public IEnumerable<TransactionType> GetAll()
        {
            IList<TransactionType> lstTransactionTypes; 

            lstTransactionTypes = new List<TransactionType>();

            using (var db = new TransEntities())
            {
                lstTransactionTypes = db.TransactionType.ToList();
            }

            return lstTransactionTypes;
        }
    }
}
