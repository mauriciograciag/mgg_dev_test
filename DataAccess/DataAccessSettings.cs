﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    /// <summary>
    /// Handles the custom settings of the DataAcess layer/proyect
    /// </summary>
    public static class DataAccessSettings
    {
        public static bool IsMocking { set; get; } = true;
    }
}
