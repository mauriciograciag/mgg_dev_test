# 
Simple Fraud Flagging System 

The General System Diagram is here

https://bitbucket.org/mauriciograciag/mgg_dev_test/src/master/files/GeneralSystemDiagram.png

Installation - Deployment Steps

1) Installation Requirements 

For Server (QA/Prod)

- SQL Server version 2014 or higher
- IIS version 6 or higher (with .Net Core feature enable)
- .Net Framework version 4.0 

For Development:
- Visual Studio 2017
- git cmd / visual studio git plugin 

2) Database creation/deployment

- On the SQL Server Create a Database and name is "TransFraud"
- Open and Run the 00_FULL_DATABASE.sql file on the created databes

https://bitbucket.org/mauriciograciag/mgg_dev_test/src/master/files/00_FULL_DATABASE.sql

- Check that the tables created matched the ER Diagram:

https://bitbucket.org/mauriciograciag/mgg_dev_test/src/master/files/MER_02.PNG

3) Deploy the WebApiCore to IIS and configure it to run 

From the Visual Studio publish the WebApi project

- this should have created the c:\inetpub\wwwroot\WebApi folder
- convert that to folder to an application that uses a .NET 4.0 application pool 

4) Deploy the WebSite to IIS

From the Visual Studio publish the WebSite project

- this should have created the c:\inetpub\wwwroot\WebSite folder
- convert that to folder to an application that uses a .NET 4.0 application pool 


Simple Fraud Flagging System - Quick User Guide - MVC 

To create a transaction access the 

https://yourSite:yourPort/Transaction/Create

To search and flag fraudulente transaction access  

https://yourSite:yourPort/Transaction/Index

Where yourSite and yourPort were defined when you published the WebSite to IIS on Step 4

For any issues with these code or improvements contact - mauricioGRacia@gmail.com
