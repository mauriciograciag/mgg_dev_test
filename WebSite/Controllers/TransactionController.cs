﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebSite.Models.TransactionViewModels;
using WebSite.Services;

namespace WebSite.Controllers
{
    public class TransactionController : Controller
    {
        ITransactionSender its = new TransactionSender();

        // GET: Transaction
        public ActionResult Index()
        {
            TransactionSearchResultsViewModel tsrvm;

            tsrvm = new TransactionSearchResultsViewModel();

            return View(tsrvm);
        }

        // GET: Transaction/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Transaction/Create
        public async Task<ActionResult> Create()
        {
            CreateTransactionViewModel cvm;

            cvm = new CreateTransactionViewModel()
            {
                dicAccounts = await LoadAccounts(),
            };

            return View(cvm);
        }

        private async Task<Dictionary<Guid, string>> LoadAccounts()
        {
            Dictionary<Guid, string> dictAccounts;

            dictAccounts = new Dictionary<Guid, string>();

            //call the webapi and fill the dictionary data

            await its.GetAllBankAccounts();

            return dictAccounts;
        }

        // POST: Transaction/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Transaction/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Transaction/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Transaction/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Transaction/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}