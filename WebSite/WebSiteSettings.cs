﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite
{
    /// <summary>
    /// Contain the custom web site settiings
    /// </summary>
    public class WebSiteSettings
    {
        /*
        //http://localhost:17978/api/Accounts/
        */
        public const string WEB_API_URL = "http://localhost:17978/api/";

        public const string AccountsIndex = "Accounts/Index";

        internal static string AccountsGetAllUrl()
        {
            return WEB_API_URL + AccountsIndex;
        }
    }
}
