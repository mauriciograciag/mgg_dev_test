﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Models.TransactionViewModels;

namespace WebSite.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITransactionSender
    {
        Task GetAllTransactionTypes();

        Task GetAllBankAccounts();

        Task RegisterTransaction(CreateTransactionViewModel param);

        Task SearchTransactions(TransactionSearchFiltersViewModel tsfvm);
    }
}
