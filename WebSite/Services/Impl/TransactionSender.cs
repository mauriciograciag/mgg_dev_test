﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebSite.Models.TransactionViewModels;

namespace WebSite.Services
{
    /// <summary>
    /// This is a intemediate/proxy class used to call the Transaction Web API - see TRANS_API_URL
    /// </summary>
    public class TransactionSender : ITransactionSender
    {
        /// <summary>
        /// Gets All the Accounts
        /// </summary>
        /// <returns></returns>
        public async Task GetAllBankAccounts()
        {
            await GetRequest(WebSiteSettings.AccountsGetAllUrl());
        }

        private async Task GetRequest(string pUrl)
        {
            HttpClient client;
            HttpResponseMessage response;

            client = new HttpClient();
            response = await client.GetAsync(pUrl); //));
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();

        }
        /// <summary>
        /// Gets All the Transaction Types
        /// </summary>
        /// <returns></returns>
        public Task GetAllTransactionTypes()
        {
            throw new NotImplementedException();
        }

        public Task RegisterTransaction(CreateTransactionViewModel param)
        {
            throw new NotImplementedException();
        }

        public Task SearchTransactions(TransactionSearchFiltersViewModel tsfvm)
        {
            throw new NotImplementedException();
        }
    }
}
