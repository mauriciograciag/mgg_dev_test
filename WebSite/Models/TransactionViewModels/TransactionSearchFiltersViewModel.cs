﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Models.TransactionViewModels
{
    /// <summary>
    /// This class is a model used to search the transactions 
    /// </summary>
    public class TransactionSearchFiltersViewModel
    {
        /// <summary>
        /// The account destination name
        /// </summary>
        [Display(Name="Account Destination")]
        public string AccountDestinationNameFilter { set; get; }

        /// <summary>
        /// Date of the given transaction
        /// </summary>
        [Display(Name = "Transaction Date")]
        public DateTime TransactionDateFilter { set; get; }

        /// <summary>
        /// Is it a fraudulent transaction (detected by system...)
        /// </summary>
        [Display(Name = "IsFraud")]
        public bool ? IsFraudFilter { set; get; }


        public TransactionSearchFiltersViewModel()
        {
            TransactionDateFilter = DateTime.Now;
            AccountDestinationNameFilter = "CXXYYZZ";
        }
    }
}
