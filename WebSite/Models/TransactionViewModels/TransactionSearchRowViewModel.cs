﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Models.TransactionViewModels
{
    /// <summary>
    /// This class is a model used to search the transactions and also flagg them as fraud
    /// </summary>
    public class TransactionSearchRowViewModel
    {
        /// <summary>
        /// The account destination name
        /// </summary>
        public bool AccountDestinationName { set; get; }

        /// <summary>
        /// Date of the given transaction
        /// </summary>
        public DateTime TransactionDate { set; get; }

        /// <summary>
        /// Is it a fraudulent transaction (detected by system...)
        /// </summary>
        public bool IsFraud { set; get; }

        /// <summary>
        /// Is the transaction flagged or not as fraud by a human
        /// </summary>
        public bool IsFlaggedFraud { set; get; }
    }
}
