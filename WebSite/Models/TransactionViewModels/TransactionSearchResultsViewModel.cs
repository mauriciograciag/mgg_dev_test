﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Models.TransactionViewModels
{
    /// <summary>
    /// This class is a model used to search the transactions and also flagg them as fraud
    /// </summary>
    public class TransactionSearchResultsViewModel
    {
        public List<TransactionSearchRowViewModel> lstTransactionsSearchResults;

        public TransactionSearchFiltersViewModel searchFilters;

        public TransactionSearchResultsViewModel()
        {
            lstTransactionsSearchResults = new List<TransactionSearchRowViewModel>();

            searchFilters = new TransactionSearchFiltersViewModel();
        }
    }
}
