﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Models.TransactionViewModels
{
    public class CreateTransactionViewModel
    {
        public Dictionary<Guid, String> dicAccounts;

        /// <summary>
        /// The Account Origin Id of the transaction
        /// </summary>
        [Required]
        [Display(Name = "Cuenta Origen")]
        public string AccountOrigId { get; set; }

        /// <summary>
        /// The Account Destination Id of the transaction
        /// </summary>
        [Required]
        [Display(Name = "Cuenta Destino")]
        public string AccountDestId { get; set; }

        /// <summary>
        /// The Amount of the transaction
        /// </summary>
        [Required]
        [Display(Name = "Monto")]
        public decimal Amount { get; set; }

        /// <summary>
        /// The type of transaction
        /// </summary>
        [Required]
        [Display(Name = "Tipo de Transacción")]
        public string TransactionType { get; set; }
    }
}
