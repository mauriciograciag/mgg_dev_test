﻿using System;
using DataAccess;
using BusinessModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LayerBusiness.UnitTests
{
    [TestClass]
    public class AccountTypeTests
    {
        [TestMethod]
        public void AccountTypesGetAllTest()
        {
            List<AccountType> lstAccountTypes;

            lstAccountTypes = AccountTypesFacade.GetAll().ToList();

            Assert.IsTrue(lstAccountTypes != null);
            Assert.IsTrue(lstAccountTypes.Count > 0);
        }
    }
}
