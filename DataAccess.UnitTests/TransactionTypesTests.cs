﻿using System;
using DataAccess;
using BusinessModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LayerBusiness.UnitTests
{
    [TestClass]
    public class TransactionTypesTests
    {
        [TestMethod]
        public void TransactionTypesTestsGetAllTest()
        {
            List<TransactionType> lstAccountTypes;

            lstAccountTypes = TransactionTypesFacade.GetAll().ToList();

            Assert.IsTrue(lstAccountTypes != null);
            Assert.IsTrue(lstAccountTypes.Count > 0);
        }
    }
}
