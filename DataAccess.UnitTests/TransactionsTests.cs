﻿using System;
using DataAccess;
using BusinessModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LayerBusiness.UnitTests
{
    [TestClass]
    public class TransactionOperationTests
    {
        private string accountName = "MGG_SAVINGS_ACCT";

        [TestMethod]
        public void TransactionRegister()
        {
            Transaction trans;
            TransactionBM transBM  ;
            bool success;

            trans = new Transaction()
            {
                Id = Guid.NewGuid(),
            };

            transBM = new TransactionBM(trans);

            success = TransactionsFacade.Register(trans);

            Assert.IsTrue(success);
        }

        [TestMethod]
        public void AccountRegister()
        {
            AccountBM acc;
            bool success;

            acc = new AccountBM()
            {
                Id = new Guid(),
                Name = accountName,
                CurrentBalance = 0,
            };
            success = AccountsFacade.Register(acc);

            Assert.IsTrue(success);
            if (success)
            {
                success = AccountsFacade.Remove(acc);

                Assert.IsTrue(success);
            }
            
        }
    }
}
